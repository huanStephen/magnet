package org.eocencle.magnet.spark1.component.handler.impl;

import org.apache.commons.lang3.StringUtils;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.sql.DataFrame;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SQLContext;
import org.eocencle.magnet.core.context.Context;
import org.eocencle.magnet.core.exception.UnsupportedException;
import org.eocencle.magnet.core.mapping.InfoParam;
import org.eocencle.magnet.core.mapping.TableInfo;
import org.eocencle.magnet.core.util.CoreTag;
import org.eocencle.magnet.core.util.StrictMap;
import org.eocencle.magnet.spark1.component.handler.SparkTableDataFrameLoader;

import java.util.Properties;

/**
 * Spark数据库表作业节点类
 * @author: huan
 * @Date: 2020-03-26
 * @Description:
 */
public class SparkDataBaseTableLoader extends SparkTableDataFrameLoader {

    public SparkDataBaseTableLoader(TableInfo tableInfo) {
        super(tableInfo);
    }

    /**
     * 创建RDD
     * @Author huan
     * @Date 2020-03-26
     * @Param [df]
     * @Return org.apache.spark.api.java.JavaRDD<org.apache.spark.sql.Row>
     * @Exception
     * @Description
     **/
    @Override
    public JavaRDD<Row> createRDD(DataFrame df) {
        return df.toJavaRDD();
    }

    /**
     * 创建DataFrame
     * @Author huan
     * @Date 2020-03-26
     * @Param [context, src]
     * @Return org.apache.spark.sql.DataFrame
     * @Exception
     * @Description
     **/
    @Override
    public DataFrame createDataFrame(Context context, String src) {
        Properties properties = new Properties();
        StrictMap<InfoParam> dbParam = this.tableInfo.getConfigParams().get(CoreTag.DB).getMap();
        properties.put("user", dbParam.get(CoreTag.DB_USERNAME).getValue());
        properties.put("password", dbParam.get(CoreTag.DB_PASSWORD).getValue());
        return ((SQLContext) context.getSQLContext()).read().jdbc(this.getDBUrl(), src, properties);
    }

    /**
     * 获取数据库URL
     * @Author huan
     * @Date 2020-03-26
     * @Param []
     * @Return java.lang.String
     * @Exception
     * @Description
     **/
    private String getDBUrl() {
        StrictMap<InfoParam> dbParam = this.tableInfo.getConfigParams().get(CoreTag.DB).getMap();
        String dialect = dbParam.get(CoreTag.DB_DIALECT).getValue();
        String host = dbParam.get(CoreTag.DB_HOST).getValue();
        String port = dbParam.get(CoreTag.DB_PORT).getValue();
        String dbName = dbParam.get(CoreTag.DB_DATABASE).getValue();
        if (CoreTag.DB_DIALECT_MYSQL.equalsIgnoreCase(dialect)) {
            if (StringUtils.isBlank(port)) {
                port = CoreTag.DB_PORT_MYSQL;
            }
            return "jdbc:mysql://" + host + ":" + port + "/" + dbName;
        } else if (CoreTag.DB_DIALECT_SQLSERVER.equalsIgnoreCase(dialect)) {
            if (StringUtils.isBlank(port)) {
                port = CoreTag.DB_PORT_SQLSERVER;
            }
            return "jdbc:sqlserver://" + host + ":" + port + ";DatabaseName=" + dbName;
        } else if (CoreTag.DB_DIALECT_ORACLE.equalsIgnoreCase(dialect)) {
            if (StringUtils.isBlank(port)) {
                port = CoreTag.DB_PORT_ORACLE;
            }
            return "jdbc:oracle:thin:@" + host + ":" + port + ":" + dbName;
        }
        throw new UnsupportedException(dialect + " database dialect is not supported!");
    }
}
