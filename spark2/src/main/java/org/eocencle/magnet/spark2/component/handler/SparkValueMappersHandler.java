package org.eocencle.magnet.spark2.component.handler;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.eocencle.magnet.core.component.WorkStageHandler;
import org.eocencle.magnet.core.mapping.ValueMappersInfo;

/**
 * Spark值映射器
 * @author: huan
 * @Date: 2020-08-15
 * @Description:
 */
public interface SparkValueMappersHandler extends WorkStageHandler {
    /**
     * 值映射处理
     * @Author huan
     * @Date 2020-08-16
     * @Param [ds, valueMappersInfo]
     * @Return org.apache.spark.sql.Dataset<org.apache.spark.sql.Row>
     * @Exception
     * @Description
     */
    Dataset<Row> handle(Dataset<Row> ds, ValueMappersInfo valueMappersInfo);

}
