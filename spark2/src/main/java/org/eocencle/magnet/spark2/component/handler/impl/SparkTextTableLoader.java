package org.eocencle.magnet.spark2.component.handler.impl;

import org.apache.spark.SparkContext;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.eocencle.magnet.core.mapping.DataSourceField;
import org.eocencle.magnet.core.mapping.TableInfo;
import org.eocencle.magnet.core.util.StrictMap;
import org.eocencle.magnet.spark2.component.handler.SparkTableRDDLoader;
import org.eocencle.magnet.spark2.util.SparkUtil;

/**
 * Spark文本表加载类
 * @author: huan
 * @Date: 2020-08-15
 * @Description:
 */
public class SparkTextTableLoader extends SparkTableRDDLoader {

    public SparkTextTableLoader(TableInfo tableInfo) {
        super(tableInfo);
    }

    /**
     * 创建RDD
     * @Author huan
     * @Date 2020-08-15
     * @Param [context, src]
     * @Return org.apache.spark.api.java.JavaRDD<org.apache.spark.sql.Row>
     * @Exception
     * @Description
     **/
    @Override
    public JavaRDD<Row> createRDD(SparkContext context, String src) {
        return SparkUtil.createRDD(context, src, tableInfo.getSeparator(), this.tableInfo.getFields());
    }

    /**
     * 创建Dataset
     * @Author huan
     * @Date 2020-08-15
     * @Param [session, dst, rdd]
     * @Return org.apache.spark.sql.Dataset<org.apache.spark.sql.Row>
     * @Exception
     * @Description
     */
    @Override
    public Dataset<Row> createDataFrame(SparkSession session, StrictMap<DataSourceField> dst, JavaRDD<Row> rdd) {
        return SparkUtil.createDataFrame(session, dst, rdd);
    }
}
