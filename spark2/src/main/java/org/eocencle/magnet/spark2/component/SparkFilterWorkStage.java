package org.eocencle.magnet.spark2.component;

import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.eocencle.magnet.core.component.*;
import org.eocencle.magnet.core.context.ComponentFactory;
import org.eocencle.magnet.spark2.component.handler.SparkFilterCondition;

import java.util.ArrayList;
import java.util.List;

/**
 * Spark过滤作业节点抽象类
 * @author: huan
 * @Date: 2020-08-15
 * @Description:
 */
public class SparkFilterWorkStage extends FilterWorkStage {
    // 过滤器
    private SparkFilterCondition filterCondition;

    @Override
    public void initHandler(WorkStageHandler handler) {
        this.filterCondition = (SparkFilterCondition) handler;
    }

    @Override
    public List<WorkStageResult> execute(WorkStageParameter parameter) {
        ComponentFactory factory = WorkStageComponentBuilderAssistant.getFactory();

        SparkWorkStageResult prevResult =
                (SparkWorkStageResult) this.getParent().getPrevResult(this.filterInfo.getRef());

        // 创建Dataset
        Dataset<Row> ds = this.filterCondition.filter(this.filterInfo.getFilterFields(), prevResult.getDs());
        // 创建RDD
        JavaRDD<Row> rdd = ds.toJavaRDD();

        // 设置返回值
        SparkWorkStageResult result = (SparkWorkStageResult) factory.createWorkStageResult();
        result.setId(this.filterInfo.getId());
        result.setAlias(this.filterInfo.getAlias());
        result.setRdd(rdd);
        result.setDs(ds);
        List<WorkStageResult> list = new ArrayList<>();
        list.add(result);
        return list;
    }
}
