package org.eocencle.magnet.spark2.component.handler;

import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.eocencle.magnet.core.component.WorkStageHandler;
import org.eocencle.magnet.core.mapping.RowNumInfo;

/**
 * Spark行号生成器
 * @author: huan
 * @Date: 2020-08-15
 * @Description:
 */
public interface SparkRowNumHandler extends WorkStageHandler {
    /**
     * 创建RDD
     * @Author huan
     * @Date 2020-08-15
     * @Param [ds]
     * @Return org.apache.spark.api.java.JavaRDD<org.apache.spark.sql.Row>
     * @Exception
     * @Description
     */
    JavaRDD<Row> createRDD(Dataset<Row> ds);
    /**
     * 创建Dataset
     * @Author huan
     * @Date 2020-08-16
     * @Param [session, rowNumInfo, ds, rdd]
     * @Return org.apache.spark.sql.Dataset<org.apache.spark.sql.Row>
     * @Exception
     * @Description
     */
    Dataset<Row> createDataset(SparkSession session, RowNumInfo rowNumInfo, Dataset<Row> ds, JavaRDD<Row> rdd);
}
