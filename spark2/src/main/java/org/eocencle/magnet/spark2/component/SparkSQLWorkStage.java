package org.eocencle.magnet.spark2.component;

import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.eocencle.magnet.core.component.*;
import org.eocencle.magnet.core.context.ComponentFactory;
import org.eocencle.magnet.core.context.Context;

import java.util.ArrayList;
import java.util.List;

/**
 * sparkSQL作业节点类
 * @author: huan
 * @Date: 2020-08-15
 * @Description:
 */
public class SparkSQLWorkStage extends SQLWorkStage {

    @Override
    public void initHandler(WorkStageHandler handler) {

    }

    @Override
    public List<WorkStageResult> execute(WorkStageParameter parameter) {
        ComponentFactory factory = WorkStageComponentBuilderAssistant.getFactory();
        Context context = parameter.getContext();

        // 创建DataFrame
        Dataset<Row> ds = this.createDataFrame((SparkSession) context.getSQLContext(), this.sqlInfo.getSql());
        // 创建RDD
        JavaRDD<Row> rdd = this.createRDD(ds);

        // 设置返回值
        SparkWorkStageResult result = (SparkWorkStageResult) factory.createWorkStageResult();
        result.setId(this.sqlInfo.getId());
        result.setAlias(this.sqlInfo.getAlias());
        result.setRdd(rdd);
        result.setDs(ds);
        List<WorkStageResult> list = new ArrayList<>();
        list.add(result);
        return list;
    }

    /**
     * 创建RDD
     * @Author huan
     * @Date 2020-08-15
     * @Param [ds]
     * @Return org.apache.spark.api.java.JavaRDD<org.apache.spark.sql.Row>
     * @Exception
     * @Description
     **/
    private JavaRDD<Row> createRDD(Dataset<Row> ds) {
        return ds.toJavaRDD();
    }

    /**
     * 执行SQL
     * @Author huan
     * @Date 2020-08-16
     * @Param [session, sql]
     * @Return org.apache.spark.sql.Dataset<org.apache.spark.sql.Row>
     * @Exception
     * @Description
     */
    private Dataset<Row> createDataFrame(SparkSession session, String sql) {
        return session.sql(sql);
    }
}
