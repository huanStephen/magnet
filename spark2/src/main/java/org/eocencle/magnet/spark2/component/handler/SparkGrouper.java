package org.eocencle.magnet.spark2.component.handler;

import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.eocencle.magnet.core.component.WorkStageHandler;
import org.eocencle.magnet.core.mapping.GroupInfo;
import org.eocencle.magnet.spark2.component.SparkWorkStageResult;

/**
 * Spark分组器
 * @author: huan
 * @Date: 2020-08-15
 * @Description:
 */
public interface SparkGrouper extends WorkStageHandler {
    /**
     * 创建RDD
     * @Author huan
     * @Date 2020-08-15
     * @Param [groupInfo, prevResult]
     * @Return org.apache.spark.api.java.JavaRDD<org.apache.spark.sql.Row>
     * @Exception
     * @Description
     **/
    JavaRDD<Row> createRDD(GroupInfo groupInfo, SparkWorkStageResult prevResult);
    /**
     * 创建Dataset
     * @Author huan
     * @Date 2020-08-16
     * @Param [session, groupInfo, prevResult, rdd]
     * @Return org.apache.spark.sql.Dataset<org.apache.spark.sql.Row>
     * @Exception
     * @Description
     */
    Dataset<Row> createDataFrame(SparkSession session, GroupInfo groupInfo, SparkWorkStageResult prevResult, JavaRDD<Row> rdd);
}
