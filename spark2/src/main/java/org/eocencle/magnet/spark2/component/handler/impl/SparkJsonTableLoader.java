package org.eocencle.magnet.spark2.component.handler.impl;

import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.sql.*;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import org.eocencle.magnet.core.context.Context;
import org.eocencle.magnet.core.mapping.DataSourceField;
import org.eocencle.magnet.core.mapping.TableInfo;
import org.eocencle.magnet.core.util.StrictMap;
import org.eocencle.magnet.spark2.component.handler.SparkTableDataFrameLoader;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * SparkJson格式表作业节点类
 * @author: huan
 * @Date: 2020-08-15
 * @Description:
 */
public class SparkJsonTableLoader extends SparkTableDataFrameLoader {

    public SparkJsonTableLoader(TableInfo tableInfo) {
        super(tableInfo);
    }

    @Override
    public JavaRDD<Row> createRDD(Dataset<Row> ds) {
        return ds.toJavaRDD();
    }

    @Override
    public Dataset<Row> createDataFrame(Context context, String src) {
        SparkSession session = ((SparkSession) context.getSQLContext());
        Dataset<Row> ds = session.read().json(src);
        StructType schema = ds.schema();
        StrictMap<StructField> fieldStrictMap = new StrictMap<>("JSON fields");
        for (StructField field: schema.fields()) {
            fieldStrictMap.put(field.name(), field);
        }

        List<StructField> structFields = new ArrayList<StructField>();
        StrictMap<DataSourceField> targetFields = this.tableInfo.getFields();
        for (Map.Entry<String, DataSourceField> targetField: targetFields.entrySet()) {
            structFields.add(fieldStrictMap.get(targetField.getValue().getName()));
        }

        JavaRDD<Row> rdd = ds.toJavaRDD().map((Row row) -> {
            Object[] fields = new Object[targetFields.size()];
            int i = 0;
            for (Map.Entry<String, DataSourceField> targetField : targetFields.entrySet()) {
                fields[i] = row.get(row.fieldIndex(targetField.getValue().getName()));
                i++;
            }
            return RowFactory.create(fields);
        });

        return session.createDataFrame(rdd, DataTypes.createStructType(structFields));
    }
}
