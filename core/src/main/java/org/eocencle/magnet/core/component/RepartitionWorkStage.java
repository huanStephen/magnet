package org.eocencle.magnet.core.component;

import org.eocencle.magnet.core.mapping.RepartitionInfo;
import org.eocencle.magnet.core.mapping.WorkStageInfo;

/**
 * 重分区作业节点抽象类
 * @author: huan
 * @Date: 2021-05-07
 * @Description:
 */
public abstract class RepartitionWorkStage extends WorkStageComponent {
    // 分片信息类
    protected RepartitionInfo repartitionInfo;

    @Override
    public void initData(WorkStageInfo info) {
        this.repartitionInfo = (RepartitionInfo) info;
    }
}
