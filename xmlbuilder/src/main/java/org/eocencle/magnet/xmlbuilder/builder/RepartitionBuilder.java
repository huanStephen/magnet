package org.eocencle.magnet.xmlbuilder.builder;

import org.eocencle.magnet.core.mapping.RepartitionInfo;
import org.eocencle.magnet.xmlbuilder.parsing.XNode;
import org.eocencle.magnet.xmlbuilder.session.XmlProjectConfig;
import org.eocencle.magnet.xmlbuilder.util.XMLBuilderTag;

import java.util.List;

/**
 * 重分区建构类
 * @author: huan
 * @Date: 2021-05-07
 * @Description:
 */
public class RepartitionBuilder implements XMLParser {
    private static RepartitionBuilder BUILDER = new RepartitionBuilder();

    private RepartitionBuilder() {

    }

    public static RepartitionBuilder getInstance() {
        return BUILDER;
    }

    @Override
    public void parse(XNode node, XmlProjectConfig config) {
        List<XNode> nodes = node.evalNodes(XMLBuilderTag.XML_EL_REPARTITION);
        this.parseElements(nodes, config);
    }

    private void parseElements(List<XNode> nodes, XmlProjectConfig config) {
        if (null == nodes) {
            return ;
        }

        RepartitionInfo repartitionInfo = null;
        for (XNode node: nodes) {
            repartitionInfo = new RepartitionInfo();
            repartitionInfo.setId(node.getStringAttribute(XMLBuilderTag.XML_ATTR_ID));
            repartitionInfo.setAlias(node.getStringAttribute(XMLBuilderTag.XML_ATTR_ALIAS));
            repartitionInfo.setRef(node.getStringAttribute(XMLBuilderTag.XML_ATTR_REF));
            repartitionInfo.setNum(Integer.parseInt(node.getStringAttribute(XMLBuilderTag.XML_ATTR_NUM)));

            config.putWorkFlowInfo(repartitionInfo.getId(), repartitionInfo);
        }
    }
}
